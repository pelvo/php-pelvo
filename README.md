# 🎉 Pelvo

Here we are going to explain everything, which we think hard pill to swollow for folks out there.

####Setting Environment:
You need to set the two environment varibales for the authentication
```
PELVO_CLIENT_ID = xxxxxxxxxxxxxxxxx
PELVO_CLIENT_SECRET = xxxxxxxxxxxxxxxxx
```
These values can be taken from Pelvo Dashbord.

####Using SDK:
The Pelvo SDK can be used or import into your code by using by following:
```
use Pelvo\Pelvo;
```
This syntax may vary according to the framework.


##Open Banking

### 1) Get Providers
This function is used to get all the providers (usually TTPs) listed, Pelvo is providing or planning to provide in near future.

```
$providers = Pelvo::getProviders(); 
```
This is will give the following successful JSON response:

```
{
    "providers": [
        {
            "provider_code": "klarna",
            "flag": "klarna-logo.png",
            "name": "Klarna"
        },
        {
            "provider_code": "yapily",
            "flag": "yapily-logo.svg",
            "name": "Yapily"
        },
        {
            "provider_code": "ping_payment",
            "flag": "ping-payment-logo.png",
            "name": "Ping Payment"
        }
    ]
}
```

### 2) Get Institutions
This function is used to get all the institutions (usually Banks) lies under the specific provider.

```
$providerCode = "yapily"; // Retrieved in the previous call
$institutions = Pelvo::getInstitutions($providerCode);
```
This is will give the following successful JSON response, this response may vary according to the selected provider.

```
{
    "institutions": [
        {
            "id": "aibgb-sandbox",
            "name": "AIB Sandbox",
            "fullName": "AIB Sandbox",
            "countries": [
                {
                    "displayName": "United Kingdom",
                    "countryCode2": "GB"
                }
            ],
            "environmentType": "SANDBOX",
            "credentialsType": "OPEN_BANKING_UK_MANUAL",
            "media": [
                {
                    "source": "https://images.yapily.com/image/802c775a-2818-483f-a81e-8b55dea90043?size=0",
                    "type": "logo"
                },
                {
                    "source": "https://images.yapily.com/image/0896d227-7a3a-423c-988e-000c0af04dc1?size=0",
                    "type": "icon"
                }
            ],
            "features": [
                "INITIATE_ACCOUNT_REQUEST",
                "INITIATE_DOMESTIC_SINGLE_PAYMENT",
                "ACCOUNT_TRANSACTIONS_WITH_MERCHANT",
                "CREATE_INTERNATIONAL_SINGLE_PAYMENT",
                ...
            ]
        }
    ]
}
```

### 3) Authorize Institutions
This function is used to start the authorization process of the selected institution.

```
$providerCode = "yapily"; // Retrieved in the first call
$institutionCode = "aibgb-sandbox"; // Retrieved in the second call
$callback = "http://localhost:8000"; // URL where system will return on institution response

$response = Pelvo::authorizeInstitution($providerCode, $institutionCode, $callback);
```
This is will give the following successful JSON response:

```
{
    "response": {
        "error": false,
        "url": "https://ob19-auth1-ui.o3bank.co.uk/....."
    }
}
```

Open the url provided in the response into the browser and authorize the consent, after successful authorization, system will return back on your provided URL with the necessary parameters, as following sample response:

```
{   
    "flow": "2y10ylHocz62ZWnfpu6URRUWuQAnPfOxpdsFaLgoZcAT6OXceVUr9bC",
    "consent": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJJTlNUSVRVVElPTiI6Im1vZGVsby....."
}
```

### 4) Get Accounts List
This function is used to get the list of accounts of the selected institution.

```
$providerCode = "yapily"; // Retrieved in the first call
$flow = "xxxx" // Retrieved in the third call
$consent = "xxxx"; // Retrieved in the third call

$response = Pelvo::getAccounts( $providerCode, $flow, $consent);
```
This is will give the following successful JSON response, it may vary according to the selected institution:

```
{
    "data": {
        "account": {
            "id": "700004000000000000000005",
            "type": "Business - Current",
            "balance": 1017147.2,
            "currency": "GBP",
            "usageType": "BUSINESS",
            "accountType": "CURRENT",
            "nickname": "xxxx0005",
            "accountNames": [
                {
                    "name": "Octon Inc"
                }
            ],
            "accountIdentifications": [
                {
                    "type": "SORT_CODE",
                    "identification": "700001"
                },
                {
                    "type": "ACCOUNT_NUMBER",
                    "identification": "70000005"
                }
            ],
            "accountBalances": [
                {
                    "type": "EXPECTED",
                    "dateTime": "2022-09-26T06:47:07.705Z",
                    "balanceAmount": {
                        "amount": 1017147.2,
                        "currency": "GBP"
                    },
                    "creditLineIncluded": false,
                    "creditLines": []
                }
            ]
        }
    }
}
```

### 5) Initiate Payment
This function is used to initiate the payment of the selected institution.

```
$body = array(
    "provider_code"         => "Code of the selected provider",
    "institution_code"      => "Code of the selected institution",
    "callback"              => "Callback URL where system will return control",
    "flow"                  => "flow code as received in authorization",
    "account_holder_name"   => "Sender Account holder name",
    "account_number"        => "Sender Account Number",
    "amount"                => "Total amount to send",
    "currency"              => "Currency of the amount",
    "reference"             => "Some reference",
    "receiverName"          => "Name of the receiver",
    "receiverCountry"       => "Currency of amount to be received in",
    "receiverIban"          => "IBAN of the receiver"
);

$response = Pelvo::initiatePayment($body);
```
This is will give the following successful JSON response, it may vary according to the selected institution:

```
{
    "response": {
        "error": false,
        "url": "https://ob19-auth1-ui.o3......"
    }
}
```

If the response has the url in the respones, it's mean it requires the authorization again, so open this url in the browser and navigate as guided. It will return the on the callback URL with parameters, as following sample response:

```
{   
    "flow": "2y10ET5DBLem2Z7IwrJ68D7nZuBanOXzNDMcRLOgnDCQOHNkgJW7RkqN6",
    "consent": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJJTlNUSVRVVElPTiI6Im1vZGVsby....."
}
```

Now again do the payment call with the latest flow and consent code, as far this sample call:


```
$body = array(
    "provider_code"         => "Code of the selected provider",
    "institution_code"      => "Code of the selected institution",
    "callback"              => "Callback URL where system will return control",
    "flow"                  => "flow code as received in authorization",
    "consent"				=> "consent code as received in authorization",
    "account_holder_name"   => "Sender Account holder name",
    "account_number"        => "Sender Account Number",
    "amount"                => "Total amount to send",
    "currency"              => "Currency of the amount",
    "reference"             => "Some reference",
    "receiverName"          => "Name of the receiver",
    "receiverCountry"       => "Currency of amount to be received in",
    "receiverIban"          => "IBAN of the receiver"
);

$response = Pelvo::initiatePayment($body);
```

This is will give the following successful JSON response, it may vary according to the selected institution:

```
{
    "data": {
        "error": false,
        "response": {
            "meta": {
                "tracingId": "bb71c3b4ffa5481f912fd20b6d9e11cc"
            },
            "data": {
                "id": "pv3-804eebc6-6e15-4017-8140-1d7ede9b8388",
                "institutionConsentId": "sdp-3-5f29d50a-b717-4a29-bc48-def1efa482e9",
                "status": "PENDING",
                "statusDetails": {
                    "status": "PENDING",
                    "statusUpdateDate": "2022-09-26T07:08:17.305Z",
                    "multiAuthorisationStatus": {
                        "status": "AWAITING_FURTHER_AUTHORIZATION",
                        "numberOfAuthorisationRequired": 3,
                        "numberOfAuthorisationReceived": 1,
                        "lastUpdatedDateTime": "2022-09-26T07:08:17.309Z"
                    },
                    "isoStatus": {
                        "code": "PDNG",
                        "name": "Pending"
                    }
                },
                "payer": {
                    "name": "Octon Inc",
                    "accountIdentifications": [
                        {
                            "type": "SORT_CODE",
                            "identification": "700001"
                        },
                        {
                            "type": "ACCOUNT_NUMBER",
                            "identification": "70000005"
                        }
                    ]
                },
                "payeeDetails": {
                    "name": "No name",
                    "accountIdentifications": [
                        {
                            "type": "IBAN",
                            "identification": "DE39499999600000005111"
                        }
                    ]
                },
                "createdAt": "2022-09-26T07:08:17.305Z",
                "firstPaymentAmount": {
                    "amount": 0,
                    "currency": "GBP"
                },
                "firstPaymentDateTime": "2022-09-27T08:04:48.090Z",
                "numberOfPayments": 3,
                "frequency": {
                    "frequencyType": "WEEKLY",
                    "intervalWeek": 1,
                    "executionDay": 3
                }
            },
            "statusCode": 201
        }
    }
}
```