<?php

namespace Pelvo;

class Pelvo
{
    private static $PELVO_TOKEN;

    private static function getPelvoToken() {
      if(self::$PELVO_TOKEN) {
        return self::$PELVO_TOKEN;
      }
      else {
        self::getAuthorized();
        return self::getPelvoToken();
      }
    }

    private static function getAuthorized()
    {
        $body = array(
            "grant_type"    => "client_credentials",
            "client_id"     => env("PELVO_CLIENT_ID"),
            "client_secret" => env("PELVO_CLIENT_SECRET"),
            "scope"         => "*"
        );

        $response = self::callPelvo('oauth/token', 'POST', [], $body);

        if(isset($response['access_token']))
          self::$PELVO_TOKEN = 'Bearer ' . $response['access_token'];
        else self::$PELVO_TOKEN = 'Unauthorized';
    }

    private static function callPelvo(string $path, $method = 'GET', $headers = [], $body = null): mixed
    {
        $curl = curl_init();
        $options = array(
            CURLOPT_URL => env('PELVO_ENDPOINT') . $path,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => $method,
            CURLOPT_POSTFIELDS => $body ? json_encode($body) : '',
            CURLOPT_HTTPHEADER => array(
                "Accept: application/json",
                "Content-Type: application/json",
            )
        );
        if (count($headers) > 0) {
            foreach ($headers as $key => $value) {
                $options[CURLOPT_HTTPHEADER][] = $key . ": " . $value;
            }
        }
        curl_setopt_array($curl, $options);
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        if ($err) {
            return "cURL Error #:" . $err;
        }
        return json_decode($response, true);
    }

    // Registeration
    public static function register(string $phoneNumber)
    {
        $body = array(
            "phone_number" => $phoneNumber
        );

        return self::callPelvo('api/v1/register', 'POST', [], $body);
    }

    public static function verifyPhoneNumber(string $code)
    {
        return self::callPelvo('api/v1/v/' . $code);
    }

    // Onboarding
    public static function addCompany(string $registrationNumber, string $legalStatus, string $countryCode)
    {
        $body = array(
            "registration_number" => $registrationNumber,
            "legal_status" => $legalStatus,
            "country_code" => $countryCode,
        );

        return self::callPelvo('api/v1/onboarding/company', 'POST', [
          'Authorization' => self::getPelvoToken()
        ], $body);
    }

    public static function getBankIdStatus(string $qrData)
    {
        $body = array(
            "qrData" => $qrData,
        );

        return self::callPelvo('api/v1/onboarding/bankIdStatus', 'POST', [
          'Authorization' => self::getPelvoToken()
        ], $body);
    }

    public static function addCompanyDetails($registrationDate, $companyName, $vat, $address, $postalCodeCity, $website)
    {
        $body = array(
            "registration_date" => registrationDate,
            "company_name" => $companyName,
            "vat_number" => $vat,
            "address" => $address,
            "postal_code_and_city" => $postalCodeCity,
            "website_url" => $website
        );

        return self::callPelvo('api/v1/onboarding/companyDetails', 'POST', [
          'Authorization' => self::getPelvoToken()
        ], $body);
    }

    public static function addUserDetails($firstName, $lastName, $address, $city, $birthday, $email)
    {
        $body = array(
            "first_name" => $firstName,
            "last_name" => $lastName,
            "home_address" => $address,
            "city" => $city,
            "birth_day" => $birthday,
            "email" => $email,
        );

        return self::callPelvo('api/v1/onboarding/userDetails', 'POST', [
          'Authorization' => self::getPelvoToken()
        ], $body);
    }

    public static function addAccountUsage(Array $body)
    {
        return self::callPelvo('api/v1/onboarding/accountUsage', 'POST', [
          'Authorization' => self::getPelvoToken()
        ], $body);
    }

    public static function addBeneficialOwner(Array $body)
    {
        return self::callPelvo('api/v1/onboarding/beneficialOwner', 'POST', [
          'Authorization' => self::getPelvoToken()
        ], $body);
    }


    // BankID
    public static function bankIdAuth(string $ip, array $certs)
    {
        $body = array(
            "client_ip" => $ip,
            "certs" => $certs
        );

        return self::callPelvo('api/v1/bankId/auth', 'POST', [
          'Authorization' => self::getPelvoToken()
        ], $body);
    }

    public static function bankIdCollect(array $data, array $certs)
    {
        $body = array(
            "data"  => $data,
            "certs" => $certs
        );

        return self::callPelvo('api/v1/bankId/collect', 'POST', [
          'Authorization' => self::getPelvoToken()
        ], $body);
    }

    public static function bankIdAnimatedQr(string $qrString)
    {
        $body = array(
            "qrString"  => $qrString,
        );

        return self::callPelvo('api/v1/bankId/animatedQr', 'POST', [
          'Authorization' => self::getPelvoToken()
        ], $body);
    }

    // KYC
    public static function kycPersonInformation(string $personalNumber)
    {
        return self::callPelvo('api/v1/kyc/personInformation/' . $personalNumber, 'GET', [
          'Authorization' => self::getPelvoToken()
        ]);
    }

    public static function kycCompanyInformation(string $personalNumber)
    {
        return self::callPelvo('api/v1/kyc/companyInformation/' . $personalNumber, 'GET', [
          'Authorization' => self::getPelvoToken()
        ]);
    }

    public static function kycCompanyDetail(string $companyId)
    {
        return self::callPelvo('api/v1/kyc/companyDetail/' . $companyId, 'GET', [
          'Authorization' => self::getPelvoToken()
        ]);
    }

    public static function kycCompanyStructure(string $companyId)
    {
        return self::callPelvo('api/v1/kyc/companyStructure/' . $companyId, 'GET', [
          'Authorization' => self::getPelvoToken()
        ]);
    }

    // Providers
    public static function getProviders()
    {
        return self::callPelvo('api/v1/providers', 'GET', [
          'Authorization' => self::getPelvoToken()
        ]);
    }

    public static function getInstitutions(string $providerCode)
    {
        return self::callPelvo('api/v1/instituions/' . $providerCode, 'GET', [
          'Authorization' => self::getPelvoToken()
        ]);
    }

    public static function authorizeInstitution(string $providerCode, string $institutionCode, string $callback)
    {

        $body = array(
            "provider_code"    => $providerCode,
            "institution_code" => $institutionCode,
            "callback"         => $callback,
        );

        return self::callPelvo('api/v1/authorize', 'POST', [
          'Authorization' => self::getPelvoToken()
        ], $body);
    }

    public static function getAccounts(string $flow, string $consent)
    {

        $body = array(
            "flow"          => $flow,
            "consent"       => $consent,
        );

        return self::callPelvo('api/v1/accounts', 'POST', [
          'Authorization' => self::getPelvoToken()
        ], $body);
    }

    public static function initiatePayment(array $body)
    {
        return self::callPelvo('api/v1/payment', 'POST', [
          'Authorization' => self::getPelvoToken()
        ], $body);
    }


    // PSU
    public static function addPsuSender(array $body)
    {
        return self::callPelvo('api/v1/psu/sender', 'POST', [
          'Authorization' => self::getPelvoToken()
        ], $body);
    }

    public static function getPsuSender()
    {
        return self::callPelvo('api/v1/psu/sender', 'GET', [
          'Authorization' => self::getPelvoToken()
        ]);
    }

    public static function addPsuReceiver(array $body)
    {
        return self::callPelvo('api/v1/psu/receiver', 'POST', [
          'Authorization' => self::getPelvoToken()
        ], $body);
    }

    public static function getPsuReceiver()
    {
        return self::callPelvo('api/v1/psu/receiver', 'GET', [
          'Authorization' => self::getPelvoToken()
        ]);
    }

    public static function addPsuPayment(array $body)
    {
        return self::callPelvo('api/v1/psu/payment', 'POST', [
          'Authorization' => self::getPelvoToken()
        ], $body);
    }
}
